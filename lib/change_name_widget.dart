import 'package:flutter/material.dart';
import 'bg_image.dart';

class ChangeNameWidget extends StatelessWidget {
  const ChangeNameWidget({
    Key key,
    @required this.myText,
    @required TextEditingController nameController,
  }) : _nameController = nameController, super(key: key);

  final String myText;
  final TextEditingController _nameController;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        BgImg(),
        SizedBox(
          height: 50,
        ),

        Text(myText,style: TextStyle(fontSize: 20)),
        SizedBox(
          height: 50,
        ),
        Padding(padding:const EdgeInsets.all(16),
            child: TextField(
              controller: _nameController,
              keyboardType: TextInputType.phone,
              decoration: InputDecoration(hintText: "enter something here"),
            )
        )


      ],
    );
  }
}