import 'package:flutter/material.dart';
import 'drawer.dart';
// import 'change_name_widget.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'Utils/Constants.dart';
import 'package:shared_preferences/shared_preferences.dart';


class HomePage extends StatefulWidget{
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  TextEditingController _nameController = TextEditingController();
  var myText = "change me";
  String url = "https://jsonplaceholder.typicode.com/photos";
  var data;
  @override
  void initState(){
    super.initState();
    getData();
  }

  getData() async{
    var res = await http.get(Uri.parse(url),headers: {"Accept": "application/json"});
    // print(res.body);
    data = jsonDecode(res.body);
    setState(() {

    });

  }



  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        title: Text("Home"),
        actions: <Widget>[
          IconButton(icon: Icon(Icons.exit_to_app), onPressed: () async{

            Constants.prefs = await SharedPreferences.getInstance();
            print(Constants.prefs);
            Constants.prefs.setBool("loggedIn", false);

            Navigator.pushReplacementNamed(context, '/login');
    })
        ],

      ),
      body: Center(
        child:Padding(
          padding: const EdgeInsets.all(16.0),
          child:data!=null?  ListView.builder(itemBuilder: (context,index){
            return ListTile(
              title: Text(data[index]["title"]),
              leading: Image.network(data[index]["url"]),
            );
          },
            itemCount: data.length,
          ):
          Center(
            child:CircularProgressIndicator(),
          ),
        ),
      ),
      drawer:MyDrawer(),
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          myText = _nameController.text;
          setState(() {

          });
        },
        child: Icon(Icons.add),
      ),

    );

  }
}