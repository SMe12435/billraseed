import 'package:flutter/material.dart';

class BgImg extends StatelessWidget{
  Widget build(BuildContext context){
    return Image.asset("assets/bg.jpeg",
      fit:BoxFit.cover,
    );
  }
}