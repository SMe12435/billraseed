import 'package:flutter/material.dart';
import 'package:raseedbano/utils/Constants.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final userNameController = TextEditingController();
  final passwordController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: Text("Login Page"),
      // ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(16),
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Card(
                  child:Padding(
                    padding : const EdgeInsets.all(16),
                  child: Form(child:Column(
                    children: <Widget>[
                      TextFormField(decoration: InputDecoration(hintText: "Username",labelText: "username"),),
                      SizedBox(height:20,),
                      TextFormField(decoration: InputDecoration(hintText: "Password"),),

                      SizedBox(height: 20),
                      RaisedButton(onPressed:(){

                          Constants.prefs.setBool("loggedIn", true);
                          Navigator.pushReplacementNamed(context, '/home');
                      },
                          child: Text("Login",),
                          color: Colors.indigo,
                        textColor: Colors.white,


                      )
                    ],
                  )),
                ),

                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
