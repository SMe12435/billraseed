import 'package:flutter/material.dart';

class MyDrawer extends StatelessWidget{
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          DrawerHeader(
            child: Text("Hi, user", style: TextStyle(color: Colors.white),),
            decoration: BoxDecoration(
                color: Colors.indigo
            ),
          ),
          ListTile(
            leading: Icon(Icons.person),
            title: Text("Profile"),
            subtitle: Text("see your profile"),
            trailing: Icon(Icons.edit),
          ),
        ],
      ),
    );
  }
}