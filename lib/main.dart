import 'package:flutter/material.dart';
import 'package:raseedbano/login_page.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'home_page.dart';
import 'utils/Constants.dart';

Future main() async{
  WidgetsFlutterBinding.ensureInitialized();

  Constants.prefs = await SharedPreferences.getInstance();
  Constants.prefs.setBool("loggedIn", false);
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    title:"Raseed Bill",
    home: Constants.prefs.getBool("loggedIn")==true?HomePage():LoginPage(),
    theme: ThemeData(
      primarySwatch: Colors.indigo,
    ),
    routes: {
      // When navigating to the "/" route, build the FirstScreen widget.
      '/login': (context) =>LoginPage(),
      // When navigating to the "/second" route, build the SecondScreen widget.
      '/home': (context) => HomePage(),
    },
  )
  );
}




